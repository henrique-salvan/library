import Factory    from "@ioc:Adonis/Lucid/Factory";
import Book       from "App/Models/Book";
import User       from "App/Models/User";
import {DateTime} from "luxon";

export const UserFactory = Factory.define(User, ({faker}) => {
    return {
        name: `${faker.name.firstName()} ${faker.name.lastName()}`,
        login: "admin",
        password: "secret",
    };
}).build();

export const BookFactory = Factory.define(Book, ({faker}) => {
    return {
        title: faker.random.words(5),
        resume: faker.random.words(30),
        active: faker.random.boolean(),
        publishedAt: DateTime.fromJSDate(new Date())
    };
}).build();
