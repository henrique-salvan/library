import BaseSeeder from "@ioc:Adonis/Lucid/Seeder";
import User       from "App/Models/User";

export default class UserSeeder extends BaseSeeder {

    public static settings = {
        loadApp: true
    };

    public async run() {
        await User.createMany([
            {
                name: "Admin",
                login: "admin",
                password: "secret",
                role: "admin"
            },
            {
                name: "Client",
                login: "client",
                password: "secret",
                role: "client",
            }
        ]);
    }

}
