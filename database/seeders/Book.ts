import BaseSeeder    from "@ioc:Adonis/Lucid/Seeder";
import {BookFactory} from "Database/factories";

export default class BookSeeder extends BaseSeeder {

    public static settings = {
        loadApp: true
    }

    public async run() {
        await BookFactory.createMany(3);
    }

}
