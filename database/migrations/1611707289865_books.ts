import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Books extends BaseSchema {
    protected tableName = "books";

    public async up() {
        this.schema.createTable(this.tableName, (table) => {

            table.increments("id");
            table.timestamps(true);

            table.string("title").notNullable();
            table.text("resume").notNullable();
            table.date("published_at");
            table.boolean("active").notNullable().defaultTo(false);

        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}
