import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Comments extends BaseSchema {
    protected tableName = "comments";

    public async up() {
        this.schema.createTable(this.tableName, (table) => {

            table.increments("id");
            table.timestamps(true);

            table.text("comment").notNullable();

            table.integer("user_id").notNullable();
            table.foreign("user_id").references("id").inTable("users").onDelete("CASCADE");
            table.index("user_id");

            table.integer("book_id").notNullable();
            table.foreign("book_id").references("id").inTable("books").onDelete("CASCADE");
            table.index("book_id");

        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}
