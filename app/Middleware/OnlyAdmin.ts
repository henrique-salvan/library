import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class OnlyAdmin {
  public async handle ({auth}: HttpContextContract, next: () => Promise<void>) {
    if (await auth.check()) {
        let userRole = auth.use("api").user?.role;
        if (userRole !== "admin") {
            return false;
        }
    }
    await next();
  }
}
