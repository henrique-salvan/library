import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import {rules, schema}       from "@ioc:Adonis/Core/Validator";

export default class CommentValidator {

    constructor(protected ctx: HttpContextContract) {
    }

    public schema = schema.create({
        comment: schema.string(),
        book_id: schema.number([
            rules.exists({table: "books", column: "id"})
        ]),
        logged_user_id: schema.number([
            rules.exists({table: "users", column: "id"})
        ]),
    });

    public messages = {};

}
