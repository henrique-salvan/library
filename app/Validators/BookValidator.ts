import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import {schema}              from "@ioc:Adonis/Core/Validator";

export default class BookValidator {

    constructor(protected ctx: HttpContextContract) {
    }

    public schema = schema.create({
        title: schema.string(),
        resume: schema.string(),
        active: schema.boolean(),
    });

    public messages = {};

}
