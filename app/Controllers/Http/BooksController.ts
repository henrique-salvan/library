import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import BookRepository        from "App/Repositories/BookRepository";
import BookValidator         from "App/Validators/BookValidator";

export default class BooksController {

    constructor(private bookRepository: BookRepository) {
        this.bookRepository = new BookRepository();
    }

    public async index() {
        return {content: await this.bookRepository.index(), success: true};
    }

    public async store({request}: HttpContextContract) {

        const data = await request.validate(BookValidator);
        const book = await this.bookRepository.store(data);

        return {content: book, success: !!book};

    }

    public async update({request, params}: HttpContextContract) {

        const data = await request.validate(BookValidator);
        const book = await this.bookRepository.update(params.id, data);

        return {content: book, success: !!book};

    }

    public async show({params}: HttpContextContract) {
        const book = await this.bookRepository.show(params.id);
        return {content: book, success: !!book};
    }

    public async destroy({params}: HttpContextContract) {
        const deleted = await this.bookRepository.destroy(params.id);
        return {content: deleted, success: deleted};
    }
}
