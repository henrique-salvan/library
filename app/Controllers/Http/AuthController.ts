import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";

export default class AuthController {

    public async login({request, auth}: HttpContextContract) {

        const login = request.input("login");
        const password = request.input("password");

        const token = await auth.use("api").attempt(login, password, {
            expiresIn: "1 day",
        });

        return {content: token.toJSON(), success: true};

    }

    public async logout({auth}: HttpContextContract) {
        await auth.use("api").logout();
        return {content: true, success: true};
    }

    public async acknowledge({auth}: HttpContextContract) {

        if (await auth.check()) {
            const user = auth.use("api").user;
            if (user) {
                return {content: user.toJSON(), success: true};
            }
        }

        return {content: false, success: false};

    }

}
