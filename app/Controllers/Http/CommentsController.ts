import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import CommentRepository     from "App/Repositories/CommentRepository";
import CommentValidator      from "App/Validators/CommentValidator";

export default class CommentsController {

    constructor(private commentRepository: CommentRepository) {
        this.commentRepository = new CommentRepository();
    }

    public async index({params}: HttpContextContract) {
        return {
            content: await this.commentRepository.index(params.bookId), success: true
        };
    }

    public async store({request}: HttpContextContract) {

        const data = await request.validate(CommentValidator);
        const comment = await this.commentRepository.store(data);

        return {content: comment, success: !!comment};

    }

    public async update({request, params}: HttpContextContract) {

        const data = await request.validate(CommentValidator);
        const comment = await this.commentRepository.update(params.id, data);

        return {content: comment, success: !!comment};

    }

    public async destroy({params}: HttpContextContract) {
        const deleted = await this.commentRepository.destroy(params.id);
        return {content: deleted, success: deleted};
    }

}
