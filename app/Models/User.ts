import Hash                            from "@ioc:Adonis/Core/Hash";
import {BaseModel, beforeSave, column} from "@ioc:Adonis/Lucid/Orm";
import {DateTime}                      from "luxon";

export default class User extends BaseModel {

    @column({isPrimary: true})
    public id: number;

    @column.dateTime({autoCreate: true})
    public createdAt: DateTime;

    @column.dateTime({autoCreate: true, autoUpdate: true})
    public updatedAt: DateTime;

    @column()
    public name: string;

    @column()
    public login: string;

    @column({serializeAs: null})
    public password: string;

    @column()
    public role: string;

    @beforeSave()
    public static async hashPassword(user: User) {
        if (user.$dirty.password) {
            user.password = await Hash.make(user.password);
        }
    }

}
