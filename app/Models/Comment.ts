import {BaseModel, belongsTo, BelongsTo, column} from "@ioc:Adonis/Lucid/Orm";
import Book                                      from "App/Models/Book";
import User                                      from "App/Models/User";
import {DateTime}                                from "luxon";

export default class Comment extends BaseModel {

    @column({isPrimary: true})
    public id: number;

    @column.dateTime({autoCreate: true})
    public createdAt: DateTime;

    @column.dateTime({autoCreate: true, autoUpdate: true})
    public updatedAt: DateTime;

    @column()
    public comment: string;

    @column()
    public userId: number;

    @column()
    public bookId: number;

    @belongsTo(() => User)
    public user: BelongsTo<typeof User>;

    @belongsTo(() => Book)
    public book: BelongsTo<typeof Book>;

}
