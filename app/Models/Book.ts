import {BaseModel, column, hasMany, HasMany, scope} from "@ioc:Adonis/Lucid/Orm";
import Comment                                      from "App/Models/Comment";
import {DateTime}                                   from "luxon";

export default class Book extends BaseModel {

    @column({isPrimary: true})
    public id: number;

    @column.dateTime({autoCreate: true})
    public createdAt: DateTime;

    @column.dateTime({autoCreate: true, autoUpdate: true})
    public updatedAt: DateTime;

    @column()
    public title: string;

    @column()
    public resume: string;

    @column.date()
    public publishedAt: DateTime;

    @column()
    public active: boolean;

    @hasMany(() => Comment)
    public comments: HasMany<typeof Comment>;

    public static activated = scope((query) => {
        query.where("active", "=", true);
    });

}
