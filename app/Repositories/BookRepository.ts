import Book from "App/Models/Book";

export default class BookRepository {

    public index() {
        return Book.query().apply(scopes => scopes.activated()).orderBy("id", "desc");
    }

    public async store(data) {
        const book = await this.fill(data).save();
        return book.$isPersisted ? book : false;
    }

    public async update(bookId: number, data) {

        const book = await Book.query().where("id", "=", bookId).firstOrFail();

        if (book) {
            this.fill(data, book);
            return await book.save();
        }

        return false;

    }

    public async show(bookId: number) {

        const book = await Book.query().where("id", "=", bookId).firstOrFail();

        if (book) {
            return book;
        }

        return false;

    }

    public async destroy(bookId: number) {

        const book = await Book.query().where("id", "=", bookId).firstOrFail();

        if (book) {
            await book.delete();
            return true;
        }

        return false;

    }

    private fill(data, book?: Book): Book {

        book = book || new Book();

        book.title = data.title;
        book.resume = data.resume;
        book.active = data.active;
        book.publishedAt = data.published_at;

        return book;

    }

}
