import Comment from "App/Models/Comment";

export default class CommentRepository {

    public async index(bookId: number) {
        return Comment.query().where("book_id", "=", bookId).preload("user").orderBy("id", "desc");
    }

    public async store(data) {
        const comment = await this.fill(data).save();
        return comment.$isPersisted ? comment : false;
    }

    public async update(commentId: number, data) {

        const comment = await Comment.query().where("id", "=", commentId).firstOrFail();

        if (comment) {
            this.fill(data, comment);
            return await comment.save();
        }

        return false;

    }

    public async destroy(commentId: number) {

        const comment = await Comment.query().where("id", "=", commentId).firstOrFail();

        if (comment) {
            await comment.delete();
            return true;
        }

        return false;

    }

    private fill(data, comment?: Comment): Comment {

        comment = comment || new Comment();

        comment.bookId = data.book_id;
        comment.userId = data.logged_user_id;
        comment.comment = data.comment;

        return comment;

    }

}
