import Route from "@ioc:Adonis/Core/Route";

Route.group(() => {

    Route.post("/auth/login", "AuthController.login");
    Route.get("/auth/logout", "AuthController.logout");
    Route.get("/auth/acknowledge", "AuthController.acknowledge");

    Route.group(() => {
        Route.resource("books", "BooksController").apiOnly().only(["store", "update", "destroy"]);
    }).middleware("onlyAdmin");

    Route.group(() => {
        Route.resource("books", "BooksController").apiOnly().only(["index", "show"]);
        Route.resource("comments", "CommentsController").apiOnly().only(["store"]);
        Route.get("comments/:bookId", "CommentsController.index");
    }).middleware("auth");

}).prefix("/api");
